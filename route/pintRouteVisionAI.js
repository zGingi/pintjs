const express = require("express");
const router = express.Router();
const Account = require("../models/Account");
const connect = require("../dbconnect");

router.route("/").post((req, res, next) => {
  console.log("Entering Method: POST pintRoute");
  res.setHeader("Content-Type", "application/json");
  const vision = require('@google-cloud/vision');
  const client = new vision.ImageAnnotatorClient({ keyFilename: "./auth/BernHackt-c25f235ad887.json"});

  let data = req.body.image;

  let buff = new Buffer(data, 'base64');
  const request = {
    image: {
      content: buff
    },
  };
  client.labelDetection(request).then(response => {
    console.log(response);
    response[0].labelAnnotations.forEach(label => console.log(label));

    if (response[0].labelAnnotations.length > 0) {
      // When something was recognised, we say OK
      req.app.socket.emit('bottle inserted', { bottle: 1, matches: response[0].labelAnnotations });
      res.statusCode = 200;
      let userID = "zGingi";
      connect.then(db => {
        let data = Account.findOne({ UserID: userID }).then(account => {
          console.log(account);
          //save Account Count to the database
          if (account && account.Counter) {
            account.Counter += parseInt(1);
            account.save();
          } else {
            account = { UserID: userID, Counter: 1 };
            let newAccount = new Account(account);
            newAccount.save();
          }
        });
      });
      res.send({ successful: true });
    } else {
      req.app.socket.emit('bottle inserted', { bottle: 0, matches: [{ description: "Objekt nicht erkannt", score: 0 }] });
      res.statusCode = 418;
      res.send({ successful: true });
    }

  }).catch(err => {
    console.error(err);
  });

});

module.exports = router;
