var socket = io();
var messages = document.getElementById("messages");

(function () {
  $("form").submit(function (e) {
    let li = document.createElement("li");
    e.preventDefault(); // prevents page reloading

    socket.emit("bottle inserted", { UserID: "zGingi", Counter: $("#message").val() });

    messages.appendChild(li).append($("#message").val());
    let span = document.createElement("span");
    messages.appendChild(span).append("by " + "Anonymous" + ": " + "just now");

    $("#message").val("");

    return false;
  });

  socket.on("bottle inserted", data => {
    let li = document.createElement("li");
    let span = document.createElement("span");
    var messages = document.getElementById("messages");
    messages.appendChild(li).append("Bottle inserted");
    messages.appendChild(span).append("by " + "zGingi" + ": " + "just now");
  });
})();

// fetching initial Account Count from the database
(function () {
  socket.on('connect', () => {
    socket.emit("start session", { UserID: "UserClient", PintID: "DeviceXY" });
  }); 

  fetch("/accounts").then(data => {
    return data.json();
  }).then(json => {
    json.map(data => {
      let li = document.createElement("li");
      let span = document.createElement("span");
      messages.appendChild(li).append(data.UserID + ", Count: " + data.Counter);
    });
  });
})();