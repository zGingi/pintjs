const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const accountSchema = new Schema(
  {
    UserID: {
      type: String
    },
    Counter: {
      type: Number
    }
  },
  {
    timestamps: true
  }
);

let Account = mongoose.model("theAccount", accountSchema);

module.exports = Account;
