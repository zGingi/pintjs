const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const accountRouter = require("./route/accountRoute");
const mobileRouter = require("./route/mobileRoute");
const pintRouter = require("./route/pintRoute");
const http = require("http").Server(app);
const io = require("socket.io");

let port = process.env.PORT;
if (port == null || port == "") {
  port = 1337;
}

//bodyparser middleware
app.use(bodyParser.json({limit: '50mb'}));
//{limit: '50mb'}

//routes
app.use("/accounts", accountRouter);
app.use("/pint", pintRouter);

//set the express.static middleware
app.use(express.static(__dirname + "/public"));

//integrating socketio
socket = io(http);

//database connection
const Account = require("./models/Account");
const connect = require("./dbconnect");

var activeSession = false;

//setup event listener
socket.on("connection", socket => {
  console.log("User connected");

  socket.on("disconnect", function() {
    console.log("Entering Message DISCONNECT: User disconnected");
  });
  
  socket.on("start session", function(msg) {
    console.log("Entering Message START SESSION: ", msg);
    console.log(msg.UserID);
    if (msg && msg.UserID === "zGingi"){
      console.log("User zGingi is connected");
      activeSession = true;
      socket.broadcast.emit("login_info", { UserID: msg.UserID, SoundID: msg.SoundID });
    }
  });

  socket.on("bottle inserted", function(msg) {
    console.log("Bottle inserted with parameters: " + JSON.stringify(msg));
    if (!activeSession){
      return;
    }
  });

  socket.on("end session", function(msg){
    console.log("Entering Message END SESSION: ", msg);
    socket.broadcast.emit("logout_info", { UserID: msg.UserID });
    activeSession = false;
  })
});

http.listen(port, () => {
  console.log("Running on Port: " + port);
});

app.socket = socket;